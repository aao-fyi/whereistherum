module gitlab.com/aao-fyi/whereistherum

go 1.18

require (
	gitlab.com/aao-fyi/shock v0.0.0-20230621195012-4182a387eb7a // indirect
	gitlab.com/aao-fyi/vendor-fonts v0.0.0-20230907202037-186926f74a63 // indirect
)
